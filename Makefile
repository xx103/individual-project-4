rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version 			#rust compiler
	cargo --version 			#rust package manager
	rustfmt --version			#rust code formatter
	rustup --version			#rust toolchain manager
	clippy-driver --version		#rust linter

format:
	cargo fmt --quiet

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

watch:
	cargo lambda watch

build1: 
	cargo lambda build --bin find_max2 --release 

build2: 
	cargo lambda build --bin is_even2 --release 

deploy1:
	cargo lambda deploy find_max2

deploy2:
	cargo lambda deploy is_even2


