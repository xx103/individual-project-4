use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct Input {
    numbers: Vec<i32>,
}

#[derive(Serialize)]
struct Output {
    max_value: i32,
    numbers: Vec<i32>, 
}

async fn find_max(event: Input, _: Context) -> Result<Output, Error> {
    let max_value = event.numbers.iter().max().cloned().unwrap_or(0);
    Ok(Output { max_value, numbers: event.numbers })
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(find_max);
    lambda_runtime::run(func).await?;
    Ok(())
}
