use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct Input {
    max_value: i32,
    numbers: Vec<i32>,
}

#[derive(Serialize)]
struct Output {
    is_even: bool,
    max_value: i32,
}

async fn is_even(event: Input, _: Context) -> Result<Output, Error> {
    let is_even = event.max_value % 2 == 0;
    Ok(Output { is_even, max_value: event.max_value })
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(is_even);
    lambda_runtime::run(func).await?;
    Ok(())
}
