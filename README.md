# Individual Project 4:AWS Lambda Functions with Step Functions Orchestration

## Overview
This project consists of two AWS Lambda functions written in Rust, orchestrated by AWS Step Functions to create a data processing pipeline. The pipeline takes a list of integers, finds the maximum number, and determines if this maximum value is even.

![Video Demo](https://gitlab.com/xx103/individual-project-4/-/wikis/uploads/653a8f0fc5e2aec38e302e5e8d7298ad/video1760052423.mp4)

## Setup Instructions

### 1. IAM User Setup

- Log in to your AWS account, navigate to the IAM (Identity and Access Management) console, and create a new user
- Attach `AWSLambda_FullAccess`, `IAMFullAccess`, `AmazonDynamoDBFullAccess`, and `AmazonDynamoDBFullAccesswithDataPipeline` policies.
- Note down the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.

### 2. Project Setup

- Create a new project with `cargo lambda new project_name`.
- Update `main.rs` and `Cargo.toml`.
- Add a `.env` file with your AWS credentials and region, and ensure it's listed in `.gitignore`.
- Export your AWS credentials and region in your terminal session.


## Lambda Functions

### 1. Max Finder (`max.rs`)
This function receives a list of integers and returns the maximum value along with the original list.

#### Input:
- `numbers`: A list of integers.

#### Output:
- `max_value`: The maximum integer from the list.
- `numbers`: The original list of integers.

### 2. Even Checker (`even.rs`)
This function checks whether a given integer (maximum value from the first function) is even.

#### Input:
- `max_value`: The maximum integer obtained from the first function.
- `numbers`: List of integers (used for tracing).

#### Output:
- `is_even`: Boolean indicating if the `max_value` is even.
- `max_value`: Echoes back the maximum value for reference.

## Deployment

1. Compile each Lambda using the following command:
   ```
   cargo build --release 
   cargo lambda deploy --binary-name find_max
   cargo lambda deploy --binary-name is_even
   ```
2. Navigate to the AWS Management Console and check if the 2 lambda functions have been deployed successfully
![Function overview](screenshot/screenshot1.png)
![Function overview](screenshot/screenshot2.png)

### Workflow Configuration
To orchestrate the Lambda functions, set up a state machine in AWS Step Functions with the following steps:

1. **Define the State Machine:**
   - Start with a `Task` state that invokes the `Max Finder` Lambda function.
   - Add a `Task` state that receives the output from the `Max Finder` and invokes the `Even Checker` Lambda function.

2. **State Configuration:**
   - Use the AWS Management Console to create the states, specifying the Lambda function ARNs.
   - Configure the input and output paths to ensure data is correctly passed between the states.
   
  ```
{
  "Comment": "A simple AWS Step Functions state machine that finds the max number and checks if it is even.",
  "StartAt": "FindMax",
  "States": {
    "FindMax": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:590183988000:function:find_max2",
      "Next": "IsEven"
    },
    "IsEven": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:590183988000:function:is_even2",
      "End": true
    }
  }
}
```

![Function overview](screenshot/screenshot3.png)

![Function overview](screenshot/screenshot6.png)
3. **Permissions:**
   - Ensure that the IAM role associated with your state machine has permission to execute the Lambda functions.


## Testing

1. Invoke the Step Functions state machine with a sample payload:
   ```json
   {
      "numbers": [1, 3, 5, 7, 10,12]
   }
   ```
2. **Verify the output for correctness**
   - the output for FindMax function should be:
  ![Function overview](screenshot/screenshot4.png)
   - the output for isEven function should be:
  ![Function overview](screenshot/screenshot5.png)
